angular.module('bragboards')
.factory('Log',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/logs', 
    name:'log',
    serializer: railsSerializer(function(){
      this.resource('location', 'Location');
      this.resource('hobby', 'Hobby');
      this.resource('user', 'User');
      this.resource('pictures', 'Picture');
      this.resource('category', 'Category');
      this.resource('recordSets', 'RecordSet');
    })
  });
}]);