angular.module('bragboards')
.factory('User', ['railsResourceFactory','railsSerializer', function(railsResourceFactory,railsSerializer){
  var user = railsResourceFactory({ 
    url: apiUrl +'/users', 
    name:'user',
    serializer: railsSerializer(function(){
      this.resource('hobbies', 'Hobby');
      this.resource('friendships', 'Friendship');
      this.resource('friends','User');
      this.resource('requests','Friendship');
      this.resource('requestedFriends', 'Friendship');
      this.resource('locations', 'Location')
    })
  });
  user.prototype.name = function(){
    return this.firstName + " " +this.lastName;
  }
  return user;
}]);
//.factory('User', function($localStorage, $auth, UserAPI){
//  $localStorage = $localStorage.$default({
//    firstName: "",
//    lastName: "",
//    email: "",
//    id: "",
//    latitude: 0,
//    longitude: 0,
//    location: "",
//    dob: "",
//    role: 1
//  });
//  
//  var _login = function(login){
//    return $auth
//    .submitLogin(login)
//    .then(function(resp){
//      console.log(resp);
//      UserAPI.get(resp.data.data.id).then(function(user){
//        _setUser(user);
//        return $localStorage.user;
//      })
//      .catch(function(){
//        return false;
//      });
//    })
//    .catch(function(){
//      return false;
//    });
//  }
//  
//  var _logout = function(){
//    return $auth.signOut()
//    .then(function(){
//      $localStorage.user = null;
//      return true;
//    })
//    .catch(function(){
//      return false;
//    });
//  }
//    
//  var _register = function(register){
//    register.role = 1;
//    var login = {
//      email: register.email,
//      password: register.password
//    };
//    return $auth.submitRegistration(register)
//    .then(function(resp){
//      _login(login);
//      return true;
//    })
//    .catch(function(resp){
//      console.log(resp);
//      return false;
//    })
//  }
//  
//  var _get = function(id){
//    return UserAPI.get(id)
//    .then(function(user){
//      return user;
//    })
//    .catch(function(resp){
//      return false;
//    })
//  }
//  
//  var _setUser = function(user){
//    $localStorage.firstName = user.firstName;
//    $localStorage.lastName = user.lastname;
//    $localStorage.email = user.email;
//    $localStorage.location = user.location;
//    $localStorage.longitude = user.longitude;
//    $localStorage.latitude = user.latitude;
//    $localStorage.dob = user.dob;
//    $localStorage.role = user.role;
//    $localStorage.id = user.id;
//  }
//  
//  return {
//    login: _login,
//    logout: _logout,
//    singup: _register,
//    current: $localStorage,
//    get: _get
//  };
//});