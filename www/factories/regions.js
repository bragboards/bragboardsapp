angular.module('bragboards')
.factory('Region',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/regions', 
    name:'region',
    serializer: railsSerializer(function(){
      this.resource('locations', 'Location');
    })
  });
}])