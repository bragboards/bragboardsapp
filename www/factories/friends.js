angular.module('bragboards')
.factory('Friendship', ['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/friendships', 
    name:'friendship',
    serializer: railsSerializer(function(){
      this.resource('user', 'User');
      this.resource('friend', 'User');
      this.resource('inverse', 'Friendship');
    })
  });
}])