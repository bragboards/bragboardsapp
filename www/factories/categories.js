angular.module('bragboards')
.factory('Category',['railsResourceFactory', 'railsSerializer',function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({
    url: apiUrl +'/categories',
    name:'category',
    serializer: railsSerializer(function(){
      this.resource('hobbies','Hobby');
    })
  });
}]);