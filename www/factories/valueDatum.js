angular.module('bragboards')
.factory('ValueData',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/value_datum', 
    name:'valueData',
    serializer: railsSerializer(function(){
      this.resource('qualifier', 'Qualifier');
    })
  });
}]);