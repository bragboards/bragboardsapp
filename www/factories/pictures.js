angular.module('bragboards')
.factory('Picture',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({
    url: apiUrl + '/pictures',
    name: 'picture'
  });
}])
.factory('PictureOwner',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({
    url: apiUrl + '/picture_owners',
    name: 'pictureOwner'
  });
}]);