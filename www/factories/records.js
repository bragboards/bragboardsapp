angular.module('bragboards')
.factory('Record',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/records', 
    name:'record',
    serializer: railsSerializer(function(){
      this.resource('action', 'Action');
      this.resource('pictures', 'Picture');
    })
  });
}]);