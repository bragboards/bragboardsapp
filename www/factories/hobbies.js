angular.module('bragboards')
.factory('Hobby',['railsResourceFactory','Category', '$http',function(railsResourceFactory, Category, $http){
  var hobby = railsResourceFactory({
    url: apiUrl +'/hobbies', 
    name: 'hobby'
  });
  hobby.prototype.Category = function(){
    return Category.get(this.categoryId);
  }
  return hobby;
}])
.factory('UserHobby', ['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/user_hobbies', 
    name:'userHobby',
    serializer: railsSerializer(function(){
      this.resource('user', 'User');
      this.resource('hobby', 'Hobby');
    })
  });
}]);