angular.module('bragboards')
.factory('RecordSet',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/record_sets', 
    name:'recordSet',
    serializer: railsSerializer(function(){
      this.resource('log', 'Log');
      this.resource('location', 'Location');
      this.resource('hobby', 'Hobby');
      this.resource('user', 'User');
      this.resource('category', 'Category');
      this.resource('records', 'Record');
    })
  });
}]);