angular.module('bragboards')
.factory('Qualifier',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/qualifiers', 
    name:'qualifier',
    serializer: railsSerializer(function(){
      this.resource('action', 'Actions');
      this.resource('valueDatum', 'ValueData');
    })
  });
}]);