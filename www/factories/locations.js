angular.module('bragboards')
.factory('Location',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/locations', 
    name:'location',
    serializer: railsSerializer(function(){
//      this.resource('region', 'RegionAPI');
      this.resource('hobbies', 'Hobby');
      this.resource('user', 'User');
      this.resource('pictures', 'Picture');
    })
  });
}]);