angular.module('bragboards')
.factory('StoredUser', function($localStorage, Location, UserHobby){
 $localStorage = $localStorage.$default({
   firstName: "",
   lastName: "",
   email: "",
   id: "",
   latitude: 0,
   longitude: 0,
   location: "",
   dob: "",
   role: 1,
   hobbies:[],
   locations:[]
 });
  
  var _get = function(){
    return $localStorage;
  }
  var _set = function(user){
    $localStorage.firstName = user.firstName;
    $localStorage.lastName = user.lastname;
    $localStorage.email = user.email;
    $localStorage.location = user.location;
    $localStorage.longitude = user.longitude;
    $localStorage.latitude = user.latitude;
    $localStorage.dob = user.dob;
    $localStorage.role = user.role;
    $localStorage.id = user.id;
    Location.query({userId: user.id}).then(function(locations){
      $localStorage.locations = locations;
    });
    $localStorage.hobbies = [];
    UserHobby.query({userId: user.id}).then(function(userHobbies){
      for(i=0; i<userHobbies.length; i++){
        $localStorage.hobbies.push(userHobbies[i].hobby);
      }
    });
  }
  
  var _clear = function(){
    $localStorage.$reset();
  }
  
  var _addHobby = function(hobby){
    $localStorage.hobbies.push(hobby);
  }
  
  var _removeHobby = function(hobby){
    for(i=0; i < $localStorage.hobbies.length; i++){
      if(hobby.id == $localStorage.hobbies[i].id){
        $localStorage.hobbies.splice(i,1);
      }
    }
  }
  
  var _getHobby = function(id){
    for(i=0; i < $localStorage.hobbies.length; i++){
      if(id == $localStorage.hobbies[i].id){
        return $localStorage.hobbies[i];
      }
    }
  }
  
  var _addLocation = function(location){
    $localStorage.locations.push(location);
  }
  
  var _removeLocation = function(location){
    for(i=0; i < $localStorage.locations.length; i++){
      if(location.id == $localStorage.locations[i].id){
        $localStorage.locations.splice(i,1);
      }
    }
  }
  
  var _getLocation = function(id){
    for(i=0; i < $localStorage.locations.length; i++){
      if(id == $localStorage.locations[i].id){
        return $localStorage.locations[i];
      }
    }
  }
  
  var _updateLocation = function(location){
    for(i=0; i < $localStorage.locations.length; i++){
      if(location.id == $localStorage.locations[i].id){
        $localStorage.locations[i] = location;
      }
    }
  }
        
  return {
    get: _get,
    set: _set,
    clear: _clear,
    addHobby: _addHobby,
    removeHobby: _removeHobby,
    getHobby: _getHobby,
    addLocation: _addLocation,
    removeLocation: _removeLocation,
    updateLocation: _updateLocation,
    getLocation: _getLocation,
  };
});