angular.module('bragboards')
.factory('Action',['railsResourceFactory', 'railsSerializer', function(railsResourceFactory, railsSerializer){
  return railsResourceFactory({ 
    url: apiUrl +'/actions', 
    name:'action',
    serializer: railsSerializer(function(){
      this.resource('hobbies', 'Hobbies');
      this.resource('records', 'Records');
      this.resource('qualifiers', 'Qualifier');
    })
  });
}]);