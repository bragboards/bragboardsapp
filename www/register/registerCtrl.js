angular.module('bragboards')
.config(function($stateProvider, $urlRouterProvider,$authProvider) {
  $stateProvider
  .state('register',{
    url:'/register',
    templateUrl: 'register/new.html' 
  })
  .state('register-activities',{
    url:'/register-activities',
    templateUrl: 'register/activities.html'
  })
  .state('register-friends',{
    url:'/register-friends',
    templateUrl: 'register/friends.html'
  });
})
.controller('NewRegisterCtrl', function($rootScope, $scope, $auth, $state, $ionicPopup, $ionicHistory, User, StoredUser){
  var main = this;
  //Setup the sign up form
  main.signUpForm = {};
  main.signUpForm.latitude = new String();
  main.signUpForm.longitude = new String();
  main.googleLocation = {};

  var geocoder = new google.maps.Geocoder();
  main.updateLatLng = function(location){
    geocoder.geocode({
      address: location.formatted_address
    }, function(results, status){
      if (status == google.maps.GeocoderStatus.OK) {
        main.signUpForm.latitude = results[0].geometry.location.lat().toString();
        main.signUpForm.longitude = results[0].geometry.location.lng().toString();
        main.signUpForm.location = results[0].formatted_address;
      }
    });
  }

  main.sendForm = function(){
    main.signUpForm.role = 1;
     var loginForm = {
      email: main.signUpForm.email,
      password: main.signUpForm.password
    };
    $auth.submitRegistration(main.signUpForm)
    .then(function(resp){
      User.get(resp.data.data.id).then(function(user){
        $rootScope.user = user;
        var alertPopup = $ionicPopup.alert({
          title: "Registration Successful!",
          template: "A confirmation email has been sent to " + resp.data.data.email + "."
        }).then(function(){
          $auth.submitLogin(loginForm)
          .then(function(){
            main.signUpForm = null;
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.transitionTo("app.browse");
          })
          .catch(function(){
            var alertPopup = $ionicPopup.alert({
              title: "Auto Login Failed!",
              template: "Please login to setup your Bragboard."
            });
            $state.go('login');
          });
        });
      });
    })
    .catch(function(resp){
      var errors = "";
      var i;
      for(i=0;i<resp.data.errors.full_messages.length;i++){
        if(i!=0){errors = errors + ".\r\n";}
        errors = errors + resp.data.errors.full_messages[i];
      }
      var alertPopup = $ionicPopup.alert({
        title: "Error!",
        template: errors
      });
    });
  };
//  $rootScope.$on('auth:login-success', function(ev, user) {
//    User.get(user.id).then(function(user){
//      StoredUser.set(user);
//    })
//  });
})
.controller('ActivitiesRegisterCtrl', function($scope, User, Category, UserHobby, $rootScope, $http, $state$){
  var main = this;
  main.target = $rootScope.user;
  main.categories = {};
  main.target.hobbies = [];
  Category.query().then(function(categories){
    main.categories = categories;
  });

  main.changeHobby = function(hobby){
    var oldHobby = false;
    var i;
    for(i=0; i<main.target.hobbies.length;i++){
      if (main.target.hobbies[i].id == hobby.id){
        oldHobby = true;
      }
    }
    if(oldHobby){
      UserHobby.query({userId: main.target.id, hobbyId: hobby.id}).then(function(data){
        data[0].delete().then(function(data){
          var hobbyIndex;
          for(hobbyIndex = 0; hobbyIndex< main.target.hobbies.length; hobbyIndex++){
            if(main.target.hobbies[hobbyIndex].id == data.hobby.id){
              main.target.hobbies.splice(hobbyIndex, 1);
              break;
            }
          }
        });
      });
    }
    else{
      main.newHobby = new UserHobby({userId: main.target.id, hobbyId: hobby.id});
      main.newHobby.create().then(function(userHobby){
        main.target.hobbies.push(userHobby.hobby);
      })
    }
  }
})
.controller('FriendsRegisterCtrl', function($scope, $rootScope, User, Friendship, $state, $ionicHistory){
  var main = this;
  main.availableUsers = [];
  main.searchText;
  main.user = $rootScope.user;
  User.query().then(function(data){
    main.availableUsers = data;
    for(i=0;i<main.availableUsers.length;i++){
      if(main.availableUsers[i].id == main.user.id){
        main.availableUsers.splice(i,1);
        break;
      }
    }
    for(i=0;i<main.availableUsers.length;i++){
      main.availableUsers[i].distance = Math.sqrt(Math.pow((main.user.latitude-main.availableUsers[i].latitude),2)+Math.pow((main.user.longitude-main.availableUsers[i].longitude),2));
      main.availableUsers[i].isFriend = false;
    }
  });
  
  main.finish = function(){
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
    $state.transitionTo('app.browse');
  }

  main.requestFriend = function(user){
    if(user.isFriend){
      Friendship.query({userId: main.user.id, friendId: user.id}).then(function(data){
        console.log(data);
        data[0].delete().then(function(data){
          var friendIndex;
          for(friendIndex = 0; friendIndex< main.user.friendships.length; friendIndex++){
            if(main.user.friendships[friendIndex].id == data.id){
              main.user.friendships.splice(friendIndex, 1);
              break;
            }
          }
          for(friendIndex = 0; friendIndex< main.user.friends.length; friendIndex++){
            if(main.user.friends[friendIndex].id == data.friend.id){
              main.user.friends.splice(friendIndex, 1);
              break;
            }
          }
          for(friendIndex = 0; friendIndex < main.availableUsers.length; friendIndex++){
            if (main.availableUsers[friendIndex].id == data.friend.id) {
              main.availableUsers[friendIndex].isFriend = false;
            };
          }
        });
      });
    }
    else{
      main.newFriendship = new Friendship({userId: main.user.id, friendId: user.id});
      main.newFriendship.create().then(function(friendship){
        main.user.friends.push(friendship.friend);
        main.user.friendships.push(friendship);
        for(friendIndex = 0; friendIndex < main.availableUsers.length; friendIndex++){
          if (main.availableUsers[friendIndex].id == friendship.friend.id) {
            main.availableUsers[friendIndex].isFriend = true;
          };
        }
      })
    }
  }
});