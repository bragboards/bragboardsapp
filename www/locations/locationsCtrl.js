angular.module('bragboards')
.config(function($stateProvider, $urlRouterProvider,$authProvider) {
  $stateProvider
  .state('app.locations-map',{
    url:'/locations',
    views:{
      'tab-more':{
         templateUrl: 'locations/map.html'
      }
    }
  })
  .state('app.locations-show',{
    url:'/locations/show/:id',
    views:{
      'tab-more':{
         templateUrl: 'locations/show.html'
      }
    }
  });
})
.controller('LocationsCtrl', function(){})
.controller('MapLocationsCtrl', function($scope, User, $state, StoredUser, Location, $stateParams, $cordovaGeolocation, $ionicActionSheet, $ionicPopup){
  var main = this;
  main.locations = [];
  var geocoder = new google.maps.Geocoder;
  main.search = "";
  var marker = {};
  $scope.$on("$ionicView.beforeEnter",function(){
    main.user = StoredUser.get();
    if(main.user.locations.length == 0){
      var introPopup = $ionicPopup.alert({
            template: "<p> Tap a location on the map then enter a name to save it.</p><p>Locations marked as private will only be availabel to you and those you share the location with. </p>",
            title: 'How to Save a Location'
          });
          introPopup.then(function(data){});
    }
    var options = {timeout: 10000, enableHighAccuracy: true};
    if(!main.map){
      $cordovaGeolocation.getCurrentPosition(options).then(function(position){
          var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          main.currentLocation = latLng;
          var mapOptions = {
            center: latLng,
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.HYBRID
          }
        main.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        var myloc = new google.maps.Marker({
          clickable: false,
          icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                                          new google.maps.Size(22,22),
                                                          new google.maps.Point(0,18),
                                                          new google.maps.Point(11,11)),
          shadow: null,
          zIndex: 999,
          map: main.map
        });
        myloc.setPosition(latLng);
        main.map.locations = angular.copy(main.user.locations);
        google.maps.event.addListenerOnce(main.map, 'idle',function(){
          for(var i=0; i < main.map.locations.length; i++){

            var location = new google.maps.LatLng(main.map.locations[i].latitude, main.map.locations[i].longitude);

            main.map.locations[i].marker = new google.maps.Marker({
              map: main.map,
              animation: google.maps.Animation.DROP,
              position: location
            });
            main.map.locations[i].marker.index=i;
            google.maps.event.addListener(main.map.locations[i].marker, 'click', function(){
              main.selectMarker(this.map.locations[this.index]);
            });
          }
        });
        google.maps.event.addListener(main.map, 'click', function(event){
          main.newLocation = new Location();
          main.newLocation.latitude = event.latLng.lat();
          main.newLocation.longitude = event.latLng.lng();
          main.newLocation.userId = main.user.id;
          geocoder.geocode({'location': event.latLng}, function(results, status){
            if (status === google.maps.GeocoderStatus.OK) {
              if(results[1]){
                main.map.setZoom(10);
                main.newLocation.geolocation = results[1].formatted_address;
              }
            };
          });
          marker = new google.maps.Marker({
            map: main.map,
            animation: google.maps.Animation.DROP,
            position: event.latLng
          });
          var popup = $ionicPopup.show({
            templateUrl: 'locations/new.html',
            title: 'Add Location',
            scope: $scope,
            buttons: [
              {
                text: 'Cancel',
                onTap: function(e){
                  marker.setMap(null);
                  main.newLocation = {};
                  return false;
                }
              },
              {
                text: 'Save',
                type: 'button-positive',
                onTap: function(e){
                    return main.newLocation;
                }
              }
            ]
          });
          popup.then(function(data){
            if(data){
              main.createLocation(data);
            }
          });
        });
      });
    }
  });

    
  main.saveCurrentLocation = function(){
    main.newLocation = new Location();
    main.newLocation.latitude = main.currentLocation.lat();
    main.newLocation.longitude = main.currentLocation.lng();
    main.newLocation.userId = main.user.id;
    geocoder.geocode({'location': main.currentLocation}, function(results, status){
      if (status === google.maps.GeocoderStatus.OK) {
        if(results[1]){
          main.map.setZoom(10);
          main.newLocation.geolocation = results[1].formatted_address;
        }
      };
    });
    marker = new google.maps.Marker({
      map: main.map,
      animation: google.maps.Animation.DROP,
      position: main.currentLocation
    });
    var popup = $ionicPopup.show({
      templateUrl: 'locations/new.html',
      title: 'Save Current location',
      scope: $scope,
      buttons: [
        {
          text: 'Cancel',
          onTap: function(e){
            marker.setMap(null);
            main.newLocation = {};
            return false;
          }
        },
        {
          text: 'Save',
          type: 'button-positive',
          onTap: function(e){
              return main.newLocation;
          }
        }
      ]
    });
    popup.then(function(data){
      if(data){
        main.createLocation(data);
      }
    });
  }
  
  
  main.createLocation = function(locationParams){
    locationParams.create().then(function(data){
      StoredUser.addLocation(angular.copy(data));
      var latLng = new google.maps.LatLng(data.latitude, data.longitude);
      marker.index = main.map.locations.length;
      data.marker = marker;
      main.map.locations.push(data);
      google.maps.event.addListener(main.map.locations[main.map.locations.length-1].marker, 'click', function(){
        main.selectMarker(this.map.locations[this.index]);
      });
    });
  }

  main.flipContainer = function(){
    var container = document.getElementById("flip-container");
    var button = document.getElementById('flipButton');
    var front = document.getElementById('map');
    var back = document.getElementById('list');
    if (angular.element(container).hasClass('flip')) {
      angular.element(container).removeClass('flip');
    }
    else{
      angular.element(container).addClass('flip');
    }
  }

  main.selectMarker = function(location){
    var sheet = $ionicActionSheet.show({
      buttons:[
        {text: 'View'},
        {text: 'Share'}
      ],
      destructiveText: 'Delete',
      cancelText: 'Cancel',
      titleText: "<b>" + location.name + " - " + location.geolocation + "</b>",
      buttonClicked: function(index){
        if (index == 0) {
          $state.go('app.locations-show', {id: location.id});
          return true;
        }
        else if(index==1){
          console.log("sharing "+ location.name);
          return true;
        }
      },
      destructiveButtonClicked: function(){
        main.delete(location);
        return true;
      }
    });
  }

  main.selectButton = function(){
    var container = document.getElementById("flip-container");
    var button = document.getElementById('flipButton');
    if (!angular.element(container).hasClass('flip')) {
      if(angular.element(button).hasClass('ion-ios-world-outline')){
        angular.element(button).removeClass('ion-ios-world-outline').addClass('ion-ios-list-outline');
      }
      else{
        angular.element(button).addClass('ion-ios-list-outline');
      }
    }
    else{
      if(angular.element(button).hasClass('ion-ios-list-outline')){
        angular.element(button).removeClass('ion-ios-list-outline').addClass('ion-ios-world-outline');
      }
      else{
        angular.element(button).addClass('ion-ios-world-outline');
      }
    }
  }
  
  main.delete = function(location){
    location.marker.setMap(null);
    Location.get(location.id).then(function(resourceLocation){
      resourceLocation.delete().then(function (deletedLocation){
        StoredUser.removeLocation(deletedLocation);
        for (var i = main.map.locations.length - 1; i >= 0; i--) {
          if(main.map.locations[i].id == deletedLocation.id){
            main.map.locations.splice(i,1);
          }
        };
      });
    });
  }
})
.controller('ShowLocationCtrl', function($scope, User, StoredUser, Picture, $state, $rootScope, Location, $stateParams, $ionicModal, $cordovaFileTransfer, $cordovaCamera, $ionicPlatform, $ionicActionSheet, $ionicSlideBoxDelegate, PictureOwner){
  var main = this;
  var tempLoc = {};
  main.pictures = [];
  $scope.$on("$ionicView.beforeEnter",function(){
    tempLoc = StoredUser.getLocation($stateParams.id);
    main.location = angular.copy(tempLoc);
    $ionicModal.fromTemplateUrl('locations/edit.html', {scope: $scope}).then(function(modal) {
      $scope.modal = modal;
      main.locationForm = main.location;
      if(main.location.private){
         main.locationPrivate = "ion-ios-checkmark item-positive";
      }
      else{
        main.locationPrivate = "ion-ios-checkmark-outline";
      }
    });
    Picture.query({locationId: main.location.id}).then(function(pictures){
      main.pictures = pictures;
    });
    var latLng = new google.maps.LatLng(main.location.latitude, main.location.longitude);
    var mapOptions = {
      draggable: false,
      panControl: false,
      center: latLng,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.HYBRID
    }
    main.map = new google.maps.Map(document.getElementById("showLocMap"), mapOptions);
    google.maps.event.addListenerOnce(main.map, 'idle',function(){
      main.location.marker = new google.maps.Marker({
        map: main.map,
        animation: google.maps.Animation.DROP,
        position: latLng
      });
      main.location.marker.location = main.location;
      google.maps.event.addListener(main.location.marker, 'click', function(){
        main.selectMarker(this.location);
      });
    });
  });
  
  main.selectMarker = function(location){
    var sheet = $ionicActionSheet.show({
      buttons:[
        {text: 'Share'}
      ],
      cancelText: 'Cancel',
      buttonClicked: function(index){
        if (index == 0) {
          console.log("sharing "+ location.name);
          return true;
        }
      }
    });
  }
  


  main.EditModal = function(){
    $scope.modal.show();
  }

  main.EditLocation = function(locationForm){
    Location.get(locationForm.id).then(function(location){
      location.name = locationForm.name;
      location.description = locationForm.description;
      location.private = locationForm.private;
      location.update().then(function(updatedLocation){
        StoredUser.updateLocation(updatedLocation);
        if(main.location.private){
          main.locationPrivate = "ion-ios-checkmark icon-positive";
        }
        else{
          main.locationPrivate = "ion-ios-circle-outline";
        }
        $scope.modal.hide();
      });
    });
  }

  main.Upload = function(){
    var sheet = $ionicActionSheet.show({
      buttons:[
        {text: 'Camera'},
        {text: 'Photo Gallery'}
      ],
      cancelText: 'Cancel',
      buttonClicked: function(index){
        if (index == 0) {
          main.capturePicture();
          return true;
        }
        if (index == 1) {
          main.showCameraRoll();
          return true;
        };
      }
    });
  }

  main.capturePicture = function(){
    $ionicPlatform.ready(function() {
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : true,
        correctOrientation: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
        var newPicture = new Picture();
        newPicture.locationId = main.location.id;
        newPicture.avatar = "data:image/jpg;base64," + imageData;
        newPicture.create().then(function(picture){
          main.pictures.push(picture);
        });
      }, function(error) {
        console.error(error);
      });
    });
  }
  main.showCameraRoll = function(){
    $ionicPlatform.ready(function() {
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit : true,
        correctOrientation: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: true
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
        var newPicture = new Picture();
        newPicture.locationId = main.location.id;
        newPicture.avatar = "data:image/jpg;base64," + imageData;
        newPicture.create().then(function(picture){
          main.pictures.push(picture);
        });
      }, function(error) {
        console.error(error);
      });
    });
  }

  main.closeEdit = function() {
    $scope.modal.hide();
    main.locationForm = null;
  }

  main.showImages = function(index) {
    main.activeSlide = index;
    main.showModal('templates/images/popover.html');
  }
 
  main.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.imageModal = modal;
      $scope.imageModal.show();
    });
  }
 
  // Close the modal
  main.closeModal = function() {
    $scope.imageModal.hide();
    $scope.imageModal.remove()
  };
  main.deleteMenu = function(){
    var pictureIndex = $ionicSlideBoxDelegate.currentIndex();
    var sheet = $ionicActionSheet.show({
      buttons:[
        {text: 'Remove from ' + main.location.name}
      ],
      destructiveText: 'Delete From Everywhere',
      cancelText: 'Cancel',
      titleText: "Remove Picture",
      buttonClicked: function(index){
        if (index == 0) {
          main.deleteReference(pictureIndex);
          return true;
        }
      },
      destructiveButtonClicked: function(){
        main.deleteImage(pictureIndex);
        return true;
      }
    });
  }

  main.deleteImage = function(index){
    main.pictures[index].delete().then(function(removedPicture){
      main.pictures.splice(index ,1);
      $ionicSlideBoxDelegate.update();
    });
  }

  main.deleteReference = function(index){
    PictureOwner.query({imageableId: main.location.id, imageableType: 'Location', pictureId: main.pictures[index].id}).then(function(pictureOwner){
      pictureOwner.delete().then(function(removedPO){
        main.pictures.splice(index ,1);
        $ionicSlideBoxDelegate.update();
      });
    });
  }
});