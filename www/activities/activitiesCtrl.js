angular.module('bragboards')
.config(function($stateProvider, $urlRouterProvider,$authProvider) {
  $stateProvider
  .state('app.activities', {
    url: '/activities',
    views:{
      'tab-more':{
        templateUrl: 'activities/list.html'  
      }
    }  
  })
  .state('app.activities-edit', {
    url:'/activities-edit',
    views:{
      'tab-more':{
        templateUrl: 'activities/edit.html'
      }
    }
  });
})
.controller('ListActivitiesCtrl', function($scope, StoredUser, User, Category, UserHobby, $http, $state){
  var main  = this;
  $scope.$on('$ionicView.beforeEnter', function(){
    main.user = StoredUser.get();
    main.filter = "";
    if(main.user.hobbies.length==0){
      $state.transitionTo("app.activities-edit");
    }
    main.user.hobbies.sort(function(a,b){
      return a.id - b.id
    });
    var catIds = [];
    for(var i = 0; i<main.user.hobbies.length; i++){
      if (catIds.indexOf(main.user.hobbies[i].categoryId) == -1){
        catIds.push(main.user.hobbies[i].categoryId);
      }
    }
    
    catIds.sort(function(a,b){
      return a - b;
    });
    
    main.categories = [];
    for(var i = 0; i<catIds.length; i++){
      Category.get(catIds[i]).then(function(category){
        category.hobbies = [];
        var newCat = category;
        for(var j = 0; j < main.user.hobbies.length; j++){
          if (main.user.hobbies[j].categoryId == newCat.id){
            newCat.hobbies.push(main.user.hobbies[j]);
          }
        }
        main.categories.push(newCat);
      });
    }
  });
  
  
  main.viewHobby = function(hobby){
    
  }
})
.controller('EditActivitiesCtrl', function(StoredUser, User, Category, UserHobby, $http, $state, $scope){
  var main = this;
  
  main.categories = {};
  var i;
  var j;
  var k;
  $scope.$on('$ionicView.beforeEnter', function(){
    main.user = StoredUser.get();
    Category.query().then(function(data){
      main.categories = data;
      for(i=0;i<main.user.hobbies.length;i++){
        for(j=0;j<main.categories.length;j++){
          for(k=0;k<main.categories[j].hobbies.length;k++){
            if (main.user.hobbies[i].id == main.categories[j].hobbies[k].id) {
              main.categories[j].hobbies[k].exists = true;
              break;
            }
          }
        }
      }
    });
  });
  
  main.changeHobby = function(hobby){
    var oldHobby = false;
    var i;
    for(i=0; i<main.user.hobbies.length;i++){
      if (main.user.hobbies[i].id == hobby.id){
        oldHobby = true;
      }
    }
    if(oldHobby){
      UserHobby.query({userId: main.user.id, hobbyId: hobby.id}).then(function(data){
        data[0].delete().then(function(data){
          StoredUser.removeHobby(data.hobby);
        });
      });
    }
    else{
      main.newHobby = new UserHobby({userId: main.user.id, hobbyId: hobby.id});
      main.newHobby.create().then(function(userHobby){
        StoredUser.addHobby(userHobby.hobby);
      });
    }
  }
  main.goHome = function(){
    $state.go("app.browse");
  }
});