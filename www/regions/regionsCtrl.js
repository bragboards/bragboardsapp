angular.module('bragboards')
.config(function($stateProvider, $urlRouterProvider,$authProvider) {
  $stateProvider
  .state('app.regions',{
    url: '/regions',
    abstract: true,
    views:{
      'menuContent': {
        template: '<ion-nav-view></ion-nav-view>',
        controller: 'RegionsCtrl' 
      }
    }
  })
  .state('app.regions.list',{
    url:"",
    parent: 'app.regions',
    templateUrl: 'regions/list.html'
  })
  .state('app.regions.show',{
    url:"/:id",
    parent: 'app.regions',
    templateUrl: 'regions/show.html'
  })
  .state('app.regions.new',{
    url:"/new",
    parent: 'app.regions',
    templateUrl: 'regions/new.html'
  })
  .state('app.regions.edit',{
    url:"/:id/edit",
    parent: 'app.regions',
    templateUrl: 'regions/edit.html'
  });
})
.controller('RegionsCtrl', function($scope, User, $state, $rootScope, Region, Location, $stateParams){})
.controller('ListRegionsCtrl', function(scope, User, $state, $rootScope, Region, Location, $stateParams){
  var main = this;
  main.$on("$ionicView.beforeEnter",function(){
    User.get($rootScope.user.id).then(function(user){
      main.user = user;
      Region.query({userId: main.user.id}).then(function(regions){
        main.regions = regions;
        if (main.regions.length==0) {
          $state.transitionTo('app.regions.new');
        };
      });
    });
  });
})
.controller('ShowRegionCtrl', function($scope, User, $state, $rootScope, Region, Location, $stateParams){
})
.controller('NewRegionCtrl', function($scope, User, $state, $rootScope, Region, Location, $stateParams){
  var main = this;
  main.region = {};
  User.get($rootScope.user.id).then(function(user){
    main.user = user;
  });

  main.makeRegion = function(newRegion){
    var tempRegion = new Region();
    tempRegion.name = newRegion.name;
    tempRegion.private = newRegion.private
    tempRegion.userId = main.user.id
    tempRegion.create().then(function(region){
      $state.transitionTo('app.regions.locations.new', {regionId: region.id} );
    });
  }
})
.controller('EditRegionCtrl', function($scope, User, $state, $rootScope, Region, Location, $stateParams){
});