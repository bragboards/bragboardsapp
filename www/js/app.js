var apiUrl = 'http://www.bragboards.com/api';
//var apiUrl = 'http://192.168.2.12:3030/api';
//var apiUrl = 'http://localhost:3030/api';
angular.module('bragboards', ['ionic', 'rails', 'ng-token-auth','ipCookie', 'ngCordova', 'ngStorage', 'ion-google-place', 'ion-sticky', 'monospaced.elastic'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider,$authProvider) {
  $stateProvider
  .state('login', {
    url: '/',
    templateUrl: 'templates/login.html'
  })
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  .state('app.browse', {
    url: '/browse',
    views: {
      'tab-home': {
        templateUrl: 'templates/browse.html'
      }
    }
  })
  .state('app.notifications', {
    url: '/notifications',
    views: {
      'tab-notifications':{
        templateUrl: 'templates/notifications.html'
      }
    }
  })
  .state('app.more', {
    url: '/more',
    views: {
      'tab-more': {
        templateUrl: 'templates/more.html'
      }
    }
  });
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');

  $authProvider.configure({
    apiUrl: apiUrl,
    tokenValidationPath:     '/auth/validate_token',
    signOutUrl:              '/auth/sign_out',
    emailRegistrationPath:   '/auth',
    accountUpdatePath:       '/auth',
    accountDeletePath:       '/auth',
    confirmationSuccessUrl:  window.location.href,
    passwordResetPath:       '/auth/password',
    passwordUpdatePath:      '/auth/password',
    passwordResetSuccessUrl: window.location.href,
    emailSignInPath:         '/auth/sign_in',
    storage:                 'localStorage',
    forceValidateToken:      false,
    validateOnPageLoad:      true,
    proxyIf:                 function() { return false; },
    proxyUrl:                '/proxy',
    omniauthWindowType:      'sameWindow',
    authProviderPaths: {
      github:   '/auth/github',
      facebook: '/auth/facebook',
      google:   '/auth/google'
    },
    tokenFormat: {
      "access-token": "{{ token }}",
      "token-type":   "Bearer",
      "client":       "{{ clientId }}",
      "expiry":       "{{ expiry }}",
      "uid":          "{{ uid }}"
    },
    parseExpiry: function(headers) {
      // convert from UTC ruby (seconds) to UTC js (milliseconds)
      return (parseInt(headers['expiry']) * 1000) || null;
    },
    handleLoginResponse: function(response) {
      return response.data;
    },
    handleAccountUpdateResponse: function(response) {
      return response.data;
    },
    handleTokenValidationResponse: function(response) {
      return response.data;
    }
  });
})
.controller('AppCtrl', function($rootScope, $scope, $ionicModal, $ionicPopup, $state, $ionicHistory, User, $auth, StoredUser) {
  var main = this;

  $rootScope.$on('auth:logout-success', function(ev, user) {
    $rootScope.user = null;
    StoredUser.clear();
    main.user = null;
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
    $state.go('login');
  });
  $rootScope.$on('auth:validation-success', function(ev, user) {
    User.get(user.id).then(function(resourceUser){
      $rootScope.user = resourceUser;
      StoredUser.set(resourceUser);
      $rootScope.user.signedIn = true;
      main.user = $rootScope.user;
      
    });
  });
})
.controller('LoginCtrl', function($scope, StoredUser, $auth, $state, $rootScope, User, $ionicPopup, $cordovaSplashscreen){
  var main = this;
  $auth.validateUser().then(function(data){
    $state.go('app.browse');
  });
  
  $rootScope.$on('auth:login-success', function(ev, user) {
    User.get(user.id).then(function(resourceUser){
      $rootScope.user = resourceUser;
      StoredUser.set(resourceUser);
      $rootScope.user.signedIn = true;
      $state.go('app.browse');
    });
  });
  $rootScope.$on('auth:login-error', function(ev, data){
    var popup = $ionicPopup.alert({
      title: 'Login Error',
      template: 'Email/Password combination is incorrect, try again.'
    });
  });
})
.controller('BrowseCtrl', function($scope){})
.controller('NotificationsCtrl', function($scope){
  var main = this;
  main.notifications = [];
})
.controller('MoreCtrl', function($scope){});
