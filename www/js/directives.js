angular.module('bragboards')
.directive('googleplace', function () {
  var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
  };
  var mapping = {
      street_number: 'number',
      route: 'street',
      locality: 'city',
      administrative_area_level_1: 'state',
      country: 'country',
      postal_code: 'zip'
  };
  // https://gist.github.com/VictorBjelkholm/6687484 
  // modified to have better structure for details
  return {
      require: 'ngModel',
      scope: {
          ngModel: '=',
          details: '=?'
      },
      link: function (scope, element, attrs, model) {
          var options = {
              types: [],
              componentRestrictions: {}
          };

          scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

          google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
              var place = scope.gPlace.getPlace();
              var place = scope.gPlace.getPlace();
              var details = place.geometry && place.geometry.location ? {
                  latitude: place.geometry.location.lat(),
                  longitude: place.geometry.location.lng()
              } : {};
              var details;
              //Get each component of the address from the place details
              //and fill the corresponding field on the form.
              for (var i = 0; i < place.address_components.length; i++) {
                  var addressType = place.address_components[i].types[0];
                  if (componentForm[addressType]) {
                      var val = place.address_components[i][componentForm[addressType]];
                      details[mapping[addressType]] = val;
                  }
              }
              details.formatted = place.formatted_address;
              details.placeId = place.place_id;
              scope.$apply(function () {
                  scope.details = details; // array containing each location component
                  model.$setViewValue(element.val());
              });
          });
      }
  };
})
.directive('map', [function($cordovaGeolocation, $ionicActionSheet){
  return {
    restrict: "E",
    bindToController: true,
    scope: {
      mapId: "@",
      locationMarker: "="
    },
    controllerAs: 'ctrl',
    controller: function(){
      console.log(this);
      this.marker = {};
      this.location = this.locationMarker;
      var latLng = new google.maps.LatLng(this.location.latitude, this.location.longitude);
      var options = {timeout: 10000, enableHighAccuracy: true};
      var mapOptions = {
        center: latLng,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.HYBRID
      }
      this.map = new google.maps.Map(document.getElementById(this.mapId), mapOptions);
      google.maps.event.addListenerOnce(this.map, 'idle', function(){

        this.location.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng
        });
        this.location.marker.location = this.location;
        google.maps.event.addListener(this.location.marker, 'click', function(){
          this.selectMarker(this.location);
        });
      });
      
      this.selectMarker = function(location){
        var sheet = $ionicActionSheet.show({
          buttons:[
            {text: 'View'},
            {text: 'Share'}
          ],
          destructiveText: 'Delete',
          cancelText: 'Cancel',
          titleText: "<b>" + location.name + " - " + location.geolocation + "</b>",
          buttonClicked: function(index){
            if (index == 0) {
              $state.go('app.locations.show', {id: location.id});
              return true;
            }
            else if(index==1){
              console.log("sharing "+ location.name);
              return true;
            }
          }
        });
      }
    },
    link: function(scope, ngModel){
      
    },
    template: '<div id="{{scope.id}}" class="item map"></div>'
  };
}]);