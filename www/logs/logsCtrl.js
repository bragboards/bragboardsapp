angular.module('bragboards')
.config(function($stateProvider, $urlRouterProvider,$authProvider) {
  $stateProvider
  .state('app.logs', {
    url: '/logs',
    views:{
      'tab-logs':{
        templateUrl: 'logs/list.html'
      }
    }
  })
  .state('app.logs-show',{
    url: 'logs/show/:id',
    views:{
      'tab-logs':{
        templateUrl: 'logs/show.html'
      }
    }
  })
  .state('app.logs-edit', {
    url:'logs/edit/:id',
    views:{
      'tab-logs':{
        templateUrl: 'logs/edit.html'
      }
    }
  })
  .state('app.logs-new-location', {
    url: '/logs/new/location',
    views:{
      'tab-logs':{
        templateUrl: 'logs/new-location.html'
      }
    }
  })
  .state('app.logs-new-hobby', {
    url: '/logs/new/hobby/:locationId',
    views:{
      'tab-logs':{
        templateUrl: 'logs/new-hobby.html'
      }
    }
  }).state('app.logs-new-users', {
    url: '/logs/new/users/:log',
    views:{
      'tab-logs':{
        templateUrl: 'logs/new-users.html'
      }
    }
  });
})
.controller('ListLogsCtrl', function($scope, Log, Location, Hobby, User, Picture, StoredUser){
  var main = this;
  main.logs = [];
  main.user = StoredUser.get();
  $scope.$on('$ionicView.beforeEnter', function(){
    Log.query({userId: main.user.id}).then(function(logs){
      if(logs){
        main.logs = logs;
        if(main.logs.length == 0){
          angular.element(document.getElementById('logs-content')).removeClass('has-header');
        }
      }
    });
  });
  
  main.delete = function(log){
    log.delete().then(function(deletedLog){
      main.logs.splice(main.logs.indexOf(deletedLog),1);
    });
  }
})
.controller('ShowLogsCtrl', function($scope, $rootScope, $ionicHistory, $stateParams, $ionicActionSheet, Log, Location, Hobby, User, Picture, StoredUser, $cordovaGeolocation, $ionicPopup, RecordSet, $state, $ionicModal, Action, Record,ValueData, $ionicPlatform, $cordovaFileTransfer, $cordovaCamera, $ionicActionSheet, $ionicSlideBoxDelegate){
  var main = this;
  main.goBack = function(){
    var hist = $ionicHistory.viewHistory();
    var currentIndex = hist.currentView.index;
    var backIndex = currentIndex * -1;
    console.log(backIndex);
    $ionicHistory.goBack(backIndex);
  }
  main.options = {
    loop: false
  }
  $scope.$on('$ionicView.beforeEnter', function(){
    Log.get($stateParams.id).then(function(log){
      main.log = log;
    });
    RecordSet.query({logId: $stateParams.id}).then(function(rs){
      main.recordSets = rs;
    });
  });
  
  main.chooseAction = function(recordSet){
    var sheetButtons = main.log.hobby.actions.map(function(action){
      var string = "Add a " + action.name;
      return {text: string};
    });
    var sheet = $ionicActionSheet.show({
      buttons: sheetButtons,
      cancelText: 'Cancel',
      buttonClicked: function(index){
        main.addRecord(recordSet, main.log.hobby.actions[index],0);
        return true;
      }
    });
  }
  
  main.editOptions = function(record){
    var sheetButtons = main.log.hobby.actions.map(function(action){
      var string = "Add a " + action.name;
      return {text: string};
    });
    var sheet = $ionicActionSheet.show({
      buttons: [
        {text: "Edit"}
      ],
      destructiveText:'Delete',
      cancelText: 'Cancel',
      buttonClicked: function(index){
        if(index == 0){
          main.editRecord(record);
          return true;
        }
      },
      destructiveButtonClicked:function(){
        main.removeRecord(record); 
        return true;
      }
    });
  }
  
  main.addRecord = function(recordSet, action, updating){
    $ionicModal.fromTemplateUrl('logs/record.html',{scope: $scope}).then(function(modal){
      main.modal = modal;
      main.newRecord = new Record();
      main.newRecord.recordSetId = recordSet.id;
      main.newRecord.actionId = action.id;
      main.newRecord.valueDatumAttributes = [];
      for(var i = 0; i<action.qualifiers.length; i++){
        var valueData = {};
        valueData.id = "";
        valueData.qualifierId = action.qualifiers[i].id;
        main.newRecord.valueDatumAttributes.push(valueData);
      }
      main.newRecord.create().then(function(recordData){
        main.currentRecord = recordData;
        ValueData.query({recordId: main.currentRecord.id}).then(function(data){
          main.currentRecord.valueDatumAttributes = data;
          console.log(main.currentRecord);
        });
        main.modal.action = action;
        main.modal.updating = 0;
        main.modal.show();
      });
    });
  }
  
  main.editRecord = function(record){
   $ionicModal.fromTemplateUrl('logs/record.html',{scope: $scope}).then(function(modal){
      main.modal = modal;
      main.modal.show();
      record.valueDatumAttributes = angular.copy(record.valueDatum);
      record.valueDatum = null;
      main.currentRecord = record;
      Action.get(record.actionId).then(function(action){
        main.modal.action = action;
        main.modal.updating = 1;
      })
   });
  }
  
  main.saveRecord = function(){
    
    main.currentRecord.update().then(function(dbRecord){
      var index = -1;
      main.currentRecord = null;
      if(main.modal.updating == 0){
        for(var i = 0; i< main.recordSets.length; i++){
          if(main.recordSets[i].id == dbRecord.recordSetId){
            index = i;
            break;
          }
        }
        main.recordSets[index].records.unshift(dbRecord);
      }
    })
    main.modal.hide();
  }
  
  main.removeRecord = function(record){
    var index = -1;
    var recordIndex = -1;
    for(var i = 0; i< main.recordSets.length; i++){
      if(main.recordSets[i].id == record.recordSetId){
        index = i;
        break;
      }
    }
    for(var i = 0; i< main.recordSets[index].records.length; i++){
      if(main.recordSets[index].records[i].id == record.id){
        recordIndex = i;
        break;
      }
    }
    record.delete().then(function(removedRecord){
      main.recordSets[index].records.splice(recordIndex,1);
    });
  }
    
  main.addUsers = function(){
    $state.go('app.logs-new-users', {log: angular.toJson(main.log)});
  }
  
  $scope.$ionicGoBack = function() {
    $ionicHistory.nextViewOptions({ historyRoot: true, animation: false });
    $state.go("app.logs");
  };
  
  main.cancelModal = function(updating){
    if(!updating){
      main.newRecord.delete().then(function(data){});
    }
    else{
      main.currentRecord.valueDatum = angular.copy(main.currentRecord.valueDatumAttributes);
      main.currentRecord.valueDatumAttributes = null;
    }
    main.modal.hide();
  }
  
  main.Upload = function(record){
    var sheet = $ionicActionSheet.show({
      buttons:[
        {text: 'Camera'},
        {text: 'Photo Gallery'}
      ],
      cancelText: 'Cancel',
      buttonClicked: function(index){
        if (index == 0) {
          main.capturePicture(record);
          return true;
        }
        if (index == 1) {
          main.showCameraRoll(record);
          return true;
        };
      }
    });
  }

  main.capturePicture = function(record){
    $ionicPlatform.ready(function() {
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : true,
        correctOrientation: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: true
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
        var newPicture = new Picture();
        newPicture.recordId = record.id;
        newPicture.avatar = "data:image/jpg;base64," + imageData;
        newPicture.create().then(function(picture){
          main.currentRecord.pictures.push(picture);
        });
      }, function(error) {
        console.error(error);
      });
    });
  }
  main.showCameraRoll = function(record){
    $ionicPlatform.ready(function() {
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit : true,
        correctOrientation: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
        var newPicture = new Picture();
        newPicture.recordId = record.id;
        newPicture.avatar = "data:image/jpg;base64," + imageData;
        newPicture.create().then(function(picture){
          main.currentRecord.pictures.push(picture);
        });
      }, function(error) {
        console.error(error);
      });
    });
  }
  main.showImages = function(index, record) {
    main.activeSlide = index;
    main.activeRecord = record;
    main.showModal('logs/popover.html');
  }
 
  main.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.imageModal = modal;
      $scope.imageModal.show();
    });
  }
 
  // Close the modal
  main.closeModal = function() {
    $scope.imageModal.hide();
    $scope.imageModal.remove()
  };
  main.deleteMenu = function(record){
    var pictureIndex = $ionicSlideBoxDelegate.currentIndex();
    var sheet = $ionicActionSheet.show({
      destructiveText: 'Delete',
      cancelText: 'Cancel',
      titleText: "Delete Picture",
      destructiveButtonClicked: function(){
        main.deleteImage(pictureIndex, record);
        return true;
      }
    });
  }

  main.deleteImage = function(index, record){
    record.pictures[index].delete().then(function(removedPicture){
      record.pictures.splice(index ,1);
      $ionicSlideBoxDelegate.update();
      if(record.pictures.length == 0){
        main.closeModal();
      }
    });
  }
})
.controller('EditLogsCtrl', function($scope, $state, Log, Location, Hobby, User, Picture){
  var main = this;
  
  main.update = function(){
    $ionicHistory.backView();
  }
})
.controller('NewLogsLocCtrl', function($scope, $state, $stateParams, $ionicActionSheet, Log, Location, Hobby, User, Picture, StoredUser, $cordovaGeolocation, $ionicPopup){
  var main = this;
  main.locations = [];
  var geocoder = new google.maps.Geocoder;
  main.search = "";
  var marker = {};
  $scope.$on("$ionicView.beforeEnter",function(){
    main.user = StoredUser.get();
    if(main.user.locations.length == 0){
      var introPopup = $ionicPopup.alert({
            template: "<p> Tap a location on the map then enter a name to save it.</p><p>Locations marked as private will only be availabel to you and those you share the location with. </p>",
            title: 'How to Save a Location'
          });
          introPopup.then(function(data){});
    }
    var options = {timeout: 10000, enableHighAccuracy: true};
    if(!main.map){
      $cordovaGeolocation.getCurrentPosition(options).then(function(position){
          var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          main.currentLocation = latLng;
          var mapOptions = {
            center: latLng,
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.HYBRID
          }
          main.map = new google.maps.Map(document.getElementById("new-log-map"), mapOptions);
          var myloc = new google.maps.Marker({
          clickable: false,
          icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                                          new google.maps.Size(22,22),
                                                          new google.maps.Point(0,18),
                                                          new google.maps.Point(11,11)),
          shadow: null,
          zIndex: 999,
          map: main.map
        });
        myloc.setPosition(latLng);
        main.map.locations = angular.copy(main.user.locations);
        google.maps.event.addListenerOnce(main.map, 'idle',function(){
          for(var i=0; i < main.map.locations.length; i++){

            var location = new google.maps.LatLng(main.map.locations[i].latitude, main.map.locations[i].longitude);

            main.map.locations[i].marker = new google.maps.Marker({
              map: main.map,
              animation: google.maps.Animation.DROP,
              position: location
            });
            main.map.locations[i].marker.index=i;
            google.maps.event.addListener(main.map.locations[i].marker, 'click', function(){
              main.selectMarker(this.map.locations[this.index]);
            });
          }
        });
        google.maps.event.addListener(main.map, 'click', function(event){
          main.newLocation = new Location();
          main.newLocation.latitude = event.latLng.lat();
          main.newLocation.longitude = event.latLng.lng();
          main.newLocation.userId = main.user.id;
          geocoder.geocode({'location': event.latLng}, function(results, status){
            if (status === google.maps.GeocoderStatus.OK) {
              if(results[1]){
                main.map.setZoom(10);
                main.newLocation.geolocation = results[1].formatted_address;
              }
            };
          });
          marker = new google.maps.Marker({
            map: main.map,
            animation: google.maps.Animation.DROP,
            position: event.latLng
          });
          var popup = $ionicPopup.show({
            templateUrl: 'locations/new.html',
            title: 'Add Location',
            scope: $scope,
            buttons: [
              {
                text: 'Cancel',
                onTap: function(e){
                  marker.setMap(null);
                  main.newLocation = {};
                  return false;
                }
              },
              {
                text: 'Save',
                type: 'button-positive',
                onTap: function(e){
                    return main.newLocation;
                }
              }
            ]
          });
          popup.then(function(data){
            if(data){
              main.createLocation(data);
            }
          });
        });
      });
    }
  });
  
  main.saveCurrentLocation = function(){
    main.newLocation = new Location();
    main.newLocation.latitude = main.currentLocation.lat();
    main.newLocation.longitude = main.currentLocation.lng();
    main.newLocation.userId = main.user.id;
    geocoder.geocode({'location': main.currentLocation}, function(results, status){
      if (status === google.maps.GeocoderStatus.OK) {
        if(results[1]){
          main.map.setZoom(10);
          main.newLocation.geolocation = results[1].formatted_address;
        }
      };
    });
    marker = new google.maps.Marker({
      map: main.map,
      animation: google.maps.Animation.DROP,
      position: main.currentLocation
    });
    var popup = $ionicPopup.show({
      templateUrl: 'locations/new.html',
      title: 'Save Current location',
      scope: $scope,
      buttons: [
        {
          text: 'Cancel',
          onTap: function(e){
            marker.setMap(null);
            main.newLocation = {};
            return false;
          }
        },
        {
          text: 'Save',
          type: 'button-positive',
          onTap: function(e){
              return main.newLocation;
          }
        }
      ]
    });
    popup.then(function(data){
      if(data){
        main.createLocation(data);
      }
    });
  }

  main.createLocation = function(locationParams){
    locationParams.create().then(function(data){
      StoredUser.addLocation(angular.copy(data));
      var latLng = new google.maps.LatLng(data.latitude, data.longitude);
      marker.index = main.map.locations.length;
      data.marker = marker;
      main.map.locations.push(data);
      google.maps.event.addListener(main.map.locations[main.map.locations.length-1].marker, 'click', function(){
        main.selectMarker(this.map.locations[this.index]);
      });
      $state.go('app.logs-new-hobby', {locationId: data.id});
    });
  }

  main.flipContainer = function(){
    var container = document.getElementById("new-log-flip-container");
    var button = document.getElementById('new-log-flipButton');
    var front = document.getElementById('new-log-map');
    var back = document.getElementById('new-log-list');
    if (angular.element(container).hasClass('flip')) {
      angular.element(container).removeClass('flip');
    }
    else{
      angular.element(container).addClass('flip');
    }
  }

  main.selectMarker = function(location){
    var sheet = $ionicActionSheet.show({
      buttons:[
        {text: 'Select Location'}
      ],
      cancelText: 'Cancel',
      titleText: "<b>" + location.name + " - " + location.geolocation + "</b>",
      buttonClicked: function(index){
        if (index == 0) {
          $state.go('app.logs-new-hobby', {locationId: location.id});
          return true;
        }
      }
    });
  }
  main.selectLocation = function(location){
    $state.go('app.logs-new-hobby', {locationId: location.id});
  }

  main.selectButton = function(){
    var container = document.getElementById("new-log-flip-container");
    var button = document.getElementById('new-log-flipButton');
    if (!angular.element(container).hasClass('flip')) {
      if(angular.element(button).hasClass('ion-ios-world-outline')){
        angular.element(button).removeClass('ion-ios-world-outline').addClass('ion-ios-list-outline');
      }
      else{
        angular.element(button).addClass('ion-ios-list-outline');
      }
    }
    else{
      if(angular.element(button).hasClass('ion-ios-list-outline')){
        angular.element(button).removeClass('ion-ios-list-outline').addClass('ion-ios-world-outline');
      }
      else{
        angular.element(button).addClass('ion-ios-world-outline');
      }
    }
  }
})
.controller('NewLogsHobbyCtrl', function($scope, $state, $stateParams, Log, Location, Hobby, StoredUser, Category, UserHobby, User){
  var main = this;
  main.locationId = $stateParams.locationId;
  main.categories = {};
  var i;
  var j;
  var k;
  $scope.$on('$ionicView.beforeEnter', function(){
    main.user = StoredUser.get();
    User.get(main.user.id).then(function(user){
      main.user = user;
    })
    Category.query().then(function(data){
      main.categories = data;
      for(i=0;i<main.user.hobbies.length;i++){
        for(j=0;j<main.categories.length;j++){
          for(k=0;k<main.categories[j].hobbies.length;k++){
            if (main.user.hobbies[i].id == main.categories[j].hobbies[k].id) {
              main.categories[j].hobbies.splice(k,1);
              k--;
            }
          }
        }
      }
    });
  });
  
  main.selectHobby = function(hobby){
    if(main.user.hobbies.indexOf(hobby) == -1){
      main.newHobby = new UserHobby({userId: main.user.id, hobbyId: hobby.id});
      main.newHobby.create().then(function(userHobby){
        StoredUser.addHobby(userHobby.hobby);
      });
    }
    var log = new Log({userId: main.user.id, hobbyId: hobby.id, locationId: main.locationId});
    log.create().then(function(newLog){
      if(main.user.friends.length>0){
        $state.go('app.logs-new-users', {log: angular.toJson(newLog)});
      }else{
        $state.go('app.logs-show', {id: newLog.id});
      }
    });
  }
})
.controller('NewLogsUsersCtrl', function($scope, $state, $stateParams, $ionicActionSheet, Log, Location, Hobby, User, Picture, StoredUser, $cordovaGeolocation, $ionicPopup, RecordSet){
  var main = this;
  var j = 0;
  var i = 0;
  main.log = angular.fromJson($stateParams.log);
  $scope.$on('$ionicView.beforeEnter', function(){
    RecordSet.query({logId: main.log.id}).then(function(rs){
      main.recordSets = rs;
      main.user= StoredUser.get();
      User.get(main.user.id).then(function(returnedUser){
        main.user = returnedUser;
        if(main.user.friends.length == 0){
          $state.go('app.logs-show',{id: main.log.id});
        }else{
          var ids = main.user.friends.map(function(friend){
            return friend.id;
          });
          for(j = main.log.recordSets.length-1; j >= 0; j--){
            if(ids.indexOf(main.recordSets[j].userId) > -1){
              main.user.friends.splice(ids.indexOf(main.recordSets[j].userId),1);
            }
          }
        }
      });
    });
  });
  
  main.addUser = function(user){
    var rs = new RecordSet({userId: user.id, logId: main.log.id});
    rs.create().then(function(newRs){
      main.recordSets.push(newRs);
      var ids = main.user.friends.map(function(friend){
        return friend.id;
      });
      for(i = main.recordSets.length-1; i >= 0; i--){
        if(ids.indexOf(main.recordSets[i].userId) > -1){
          main.user.friends.splice(ids.indexOf(main.recordSets[i].userId),1);
        }
      }
    });
  }
});