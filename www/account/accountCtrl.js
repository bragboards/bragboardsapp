angular.module('bragboards')
.config(function($stateProvider, $urlRouterProvider,$authProvider) {
  $stateProvider
  .state('app.account', {
    url:'/account',
    views:{
      'tab-more': {
        templateUrl: 'account/details.html'
      }
    }
  })
  .state('app.account-update',{
    url:'/account-update',
    views:{
      'tab-more': {
        templateUrl: 'account/update.html'
      }
    }
  });
})
.controller('AccountCtrl', function($auth, StoredUser, User){
  var main = this;
  main.user = StoredUser.get();
})
.controller('UpdateAccountCtrl', function($auth){});