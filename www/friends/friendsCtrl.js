angular.module('bragboards')
.config(function($stateProvider, $urlRouterProvider,$authProvider){
  $stateProvider
  .state('app.friends', {
    url: '/friends',
    views:{
      'tab-friends': {
        templateUrl: 'friends/tabs.html'
      }
    }
  })
  .state('app.friends.list',{
    url: '/list',
    views:{
      'list-tab':{
        templateUrl: 'friends/list.html'
      }
    }
  })
  .state('app.friends.request', {
    url: '/requests',
    views:{
      'request-tab':{
        templateUrl: 'friends/requests.html'
      }
    }
  })
  .state('app.friends.find', {
    url: '/find',
    views:{
      'find-tab':{
        templateUrl: 'friends/find.html'
      }
    }
  });
})
.controller('FriendsCtrl', function(){})
.controller('ListFriendsCtrl', function($scope, User, Friendship, $state, $rootScope, $ionicPopover){
  var main = this;
  main.user = {};
  main.filter = "";
  $scope.$parent.$on("$ionicView.beforeEnter",function(){
    User.get($rootScope.user.id).then(function(user){
      main.user = user;
    });
  });

  main.handleFriend = function(action, friend){
    Friendship.query({userId: main.user.id, friendId: friend.id}).then(function(friendship){
      if (action == 'VIEW') {
        alert("You viewed " + friendship.friend.firstName);
      }
      else if(action == "MESSAGE"){
        alert("You started a chat with "+ friendship.friend.firstName);
      }
      else if(action == 'DELETE'){
        friendship.delete().then(function(removed_friendship){
          for(i=0;i<main.user.friends.length;i++){
            if (main.user.friends[i].id == removed_friendship.friendId) {
              main.user.friends.splice(i,1);
            }
          }
          for(i=0;i<main.user.friendships.length;i++){
            if (main.user.friendships[i].id == removed_friendship.id) {
              main.user.friendships.splice(i,1);
            }
          }
        });
      }
      else{
        alert('Unknown Action');
      }
    });
  }
  
  main.subheader = function(){
    if(main.user.friends.length > 0){
      return "has-subheader";
    }
    else{
      return "";
    }
  }
})
.controller('RequestFriendsCtrl', function($scope, User, Friendship, $state, $rootScope, $ionicPopover, StoredUser){
  var main = this
  main.requests = [];
  main.user = StoredUser.get();
  main.filter = "";
  $scope.$parent.$on('$ionicView.beforeEnter', function(){
    User.get(main.user.id).then(function(user){
      main.user = user;
      if (main.user.requests.length > main.requests.length) {
        main.requests = [];
        for(i=0;i<main.user.requests.length;i++){
          Friendship.get(main.user.requests[i].id).then(function(friendship){
            main.requests.push(friendship);
          });
        }
      }
      if(main.requests.length == 0){
        angular.element(document.getElementById('requests-content')).removeClass('has-header');
      }
    });
  });

  main.toggleMore = function(item, event) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }    
 
    if (main.popover) {
      main.popover.show(event);
    }
    else {
      $ionicPopover.fromTemplateUrl('friends/request-popover.html', {
        scope: $scope
      }).then(function(popover) {
        main.popover = popover;
        main.popoverItem = item;
        $scope.$on('$destroy', function() {
          main.popover.remove().then(
          function() {
            delete main.popover;
          });
        });
        main.popover.show(event);
      });
    }    
    
  }

  main.handlePopover = function(action, request, event){
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }
    if (action == 'APPROVE') {
      Friendship.get(request.id).then(function(returned_request){
        returned_request.update({friendId: returned_request.friend.id}).then(function(friendship){
          for(i=0;i<main.requests.length;i++){
            if (main.requests[i].id == friendship.inverseId) {
              main.requests.splice(i,1);
            };
          }
        });
      });
    }
    else{
      Friendship.get(request.id).then(function(returned_request){
        returned_request.delete().then(function(removed_request){
          for(i=0;i<main.requests.length;i++){
            if (main.requests[i].id == removed_request.id) {
              main.requests.splice(i,1);
            }
          }
        });
      });
    }
    main.popover.remove().then(function() {
      delete main.popover;
    });
  }

})
.controller('FindFriendsCtrl', function($scope, User, Friendship, $state, $rootScope, StoredUser){
  var main = this;
  main.availableUsers = [];
  main.filter;
  main.user = StoredUser.get();
  var i;
  $scope.$parent.$on('$ionicView.beforeEnter',function(){
    User.get(main.user.id).then(function(user){
      main.user = user;
      User.query().then(function(data){
        main.availableUsers = data;
        for(i=0;i<main.availableUsers.length;i++){
          if(main.availableUsers[i].id == main.user.id){
            main.availableUsers.splice(i,1);
            break;
          }
        }
        for(i=0;i<main.user.friends.length;i++){
          for(j=0; j<main.availableUsers.length; j++){
            if (main.availableUsers[j].id == main.user.friends[i].id) {
              main.availableUsers.splice(j,1);
              break;
            }
          }
        }
        for(i=0;i<main.user.requests.length;i++){
          for(j=0; j<main.availableUsers.length; j++){
            if (main.availableUsers[j].id == main.user.requests[i].userId) {
              main.availableUsers.splice(j,1);
            }
          }
        }
        for(i=0;i<main.user.requestedFriends.length;i++){
          for(j=0; j<main.availableUsers.length; j++){
            if (main.availableUsers[j].id == main.user.requestedFriends[i].friendId) {
              main.availableUsers[j].isFriend = true;
              break;
            }
          }
        }
        for(i=0;i<main.availableUsers.length;i++){
          main.availableUsers[i].distance = Math.sqrt(Math.pow((main.user.latitude-main.availableUsers[i].latitude),2)+Math.pow((main.user.longitude-main.availableUsers[i].longitude),2));
        }
      });
    });
  });
  

  main.requestFriend = function(user){
    if(user.isFriend){
      Friendship.query({userId: main.user.id, friendId: user.id}).then(function(data){
        data[0].delete().then(function(data){
          var friendIndex;
          for(friendIndex = 0; friendIndex< main.user.friendships.length; friendIndex++){
            if(main.user.friendships[friendIndex].id == data.id){
              main.user.friendships.splice(friendIndex, 1);
              break;
            }
          }
          for(friendIndex = 0; friendIndex< main.user.friends.length; friendIndex++){
            if(main.user.friends[friendIndex].id == data.friend.id){
              main.user.friends.splice(friendIndex, 1);
              break;
            }
          }
          for(friendIndex = 0; friendIndex < main.availableUsers.length; friendIndex++){
            if (main.availableUsers[friendIndex].id == data.friend.id) {
              main.availableUsers[friendIndex].isFriend = false;
            };
          }
        });
      });
    }
    else{
      main.newFriendship = new Friendship({userId: main.user.id, friendId: user.id});
      main.newFriendship.create().then(function(friendship){
        main.user.friends.push(friendship.friend);
        main.user.friendships.push(friendship);
        for(friendIndex = 0; friendIndex < main.availableUsers.length; friendIndex++){
          if (main.availableUsers[friendIndex].id == friendship.friend.id) {
            main.availableUsers[friendIndex].isFriend = true;
          };
        }
      })
    }
  }
});